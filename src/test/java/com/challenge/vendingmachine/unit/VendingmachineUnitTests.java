package com.challenge.vendingmachine.unit;

import com.challenge.vendingmachine.crud.VendingMachineImpl;
import com.challenge.vendingmachine.entity.CoinEnum;
import org.junit.jupiter.api.Test;

import java.util.Map;


class VendingmachineUnitTests {


	@Test
	void test() {

		VendingMachineImpl machine = new VendingMachineImpl();

		machine.userInsertCoin(CoinEnum.TENPENCE);
		String test = CoinEnum.TWOPOUNDS.toString();
		Long balance = machine.getCurrentBalance();


		Map<CoinEnum, Integer> testChange = machine.getCoinInventory();
		System.out.println(testChange.toString());
	}

}
