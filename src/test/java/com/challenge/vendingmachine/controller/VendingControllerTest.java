package com.challenge.vendingmachine.controller;

import com.challenge.vendingmachine.entity.CoinEnum;
import com.challenge.vendingmachine.crud.VendingMachineImpl;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(VendingController.class)
class VendingControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private VendingMachineImpl vendingMachine;

    @Test
    void test() throws Exception{
        //imput
        mvc.perform(post("/vendingmachine/insertcoin").content(CoinEnum.TENPENCE.toString()));


        mvc.perform(get("/vendingmachine/coins"))  //method call
           .andExpect(status().isOk())
           .andExpect(jsonPath("$.TENPENCE", is(1)))//expect output to be
           .andExpect(jsonPath("$.ONEPOUND", is(0)))//expect output to be
           .andDo(MockMvcResultHandlers.print());

    }
}