package com.challenge.vendingmachine.crud;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CashInventory<Coin> {

    private Map<Coin, Integer> cashInventory = new HashMap<Coin, Integer>();

    public int getQuantity(Coin  denomination){
        Integer value = cashInventory.get(denomination);
        return value == null? 0 : value;
    }

    public void add(Coin denomination){
        int count = cashInventory.get(denomination);
        cashInventory.put(denomination, count+1);
    }

    public void deduct(Coin denomination){
        if (hasCoin(denomination)){
            int count = cashInventory.get(denomination);
            cashInventory.put(denomination, count -1);
        }
    }
    public Map<Coin, Integer> getAllCoins(){
        return cashInventory;
    }

    public boolean hasCoin(Coin denomination){
        return getQuantity(denomination) > 0;
    }

    public void clear(){
        cashInventory.clear();
    }

    public void putCoin(Coin denomination, int quantity){
        cashInventory.put(denomination, quantity);
    }
}
