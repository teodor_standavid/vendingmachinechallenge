package com.challenge.vendingmachine.crud;

import com.challenge.vendingmachine.crud.CashInventory;
import com.challenge.vendingmachine.crud.NotSufficientChangeException;
import com.challenge.vendingmachine.crud.VendingMachine;
import com.challenge.vendingmachine.entity.CoinEnum;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class VendingMachineImpl implements VendingMachine {

    private CashInventory<CoinEnum> coinCashInventory = new CashInventory<CoinEnum>();
    private long currentBalance;

    public VendingMachineImpl() {
        initialise();
    }

    private void initialise() {
        for(CoinEnum c : CoinEnum.values()){
            coinCashInventory.putCoin(c, 0);
        }
    }

    @Override
    public void userInsertCoin(CoinEnum coinEnum) {
        currentBalance = currentBalance + coinEnum.getDenomitation();
        coinCashInventory.add(coinEnum);
    }

    @Override
    public long getCurrentBalance() {
        return this.currentBalance;
    }


    public List<CoinEnum> calculateChangeFromPrice(long productPrice) {
        return this.calculateChange(productPrice);
    }
    @Override
    public Map<CoinEnum, Integer> getCoinInventory() {
        return this.coinCashInventory.getAllCoins();
    }

    @Override
    public List<CoinEnum> calculateChange(long changeAmount) {
            List<CoinEnum> change = getChange(changeAmount);
            updateCashInventory(change);
            return change;
    }

    private List<CoinEnum> getChange(long changeAmount) throws NotSufficientChangeException {

        List<CoinEnum> changes = Collections.EMPTY_LIST;

        if(changeAmount > 0) {
            changes = new ArrayList<CoinEnum>();
            long balance = changeAmount;
            while (balance > 0) {
                if (balance >= CoinEnum.TWOPOUNDS.getDenomitation() && coinCashInventory.hasCoin(CoinEnum.TWOPOUNDS)) {
                    coinCashInventory.deduct(CoinEnum.TWOPOUNDS);
                    changes.add(CoinEnum.TWOPOUNDS);
                    balance = balance - CoinEnum.TWOPOUNDS.getDenomitation();
                    continue;
                } else if (balance >= CoinEnum.ONEPOUND.getDenomitation() && coinCashInventory.hasCoin(CoinEnum.ONEPOUND)) {
                    coinCashInventory.deduct(CoinEnum.ONEPOUND);
                    changes.add(CoinEnum.ONEPOUND);
                    balance = balance - CoinEnum.ONEPOUND.getDenomitation();
                    continue;
                } else if (balance >= CoinEnum.FIFTYPENCE.getDenomitation() && coinCashInventory.hasCoin(CoinEnum.FIFTYPENCE)) {
                    coinCashInventory.deduct(CoinEnum.FIFTYPENCE);
                    changes.add(CoinEnum.FIFTYPENCE);
                    balance = balance - CoinEnum.FIFTYPENCE.getDenomitation();
                    continue;
                } else if (balance >= CoinEnum.TWENTYPENCE.getDenomitation() && coinCashInventory.hasCoin(CoinEnum.TWENTYPENCE)) {
                    coinCashInventory.deduct(CoinEnum.TWENTYPENCE);
                    changes.add(CoinEnum.TWENTYPENCE);
                    balance = balance - CoinEnum.TWENTYPENCE.getDenomitation();
                    continue;
                } else if (balance >= CoinEnum.TENPENCE.getDenomitation() && coinCashInventory.hasCoin(CoinEnum.TENPENCE)) {
                    coinCashInventory.deduct(CoinEnum.TENPENCE);
                    changes.add(CoinEnum.TENPENCE);
                    balance = balance - CoinEnum.TENPENCE.getDenomitation();
                    continue;
                } else if (balance >= CoinEnum.FIVEPENCE.getDenomitation() && coinCashInventory.hasCoin(CoinEnum.FIVEPENCE)) {
                    coinCashInventory.deduct(CoinEnum.FIVEPENCE);
                    changes.add(CoinEnum.FIVEPENCE);
                    balance = balance - CoinEnum.FIVEPENCE.getDenomitation();
                    continue;
                } else {
                    NotSufficientChangeException();
                }
            }
        }

        return changes;
    }

    private void NotSufficientChangeException() {
        throw new NotSufficientChangeException("Insufficient change in the machine");
    }

    private void updateCashInventory(List<CoinEnum> change) {
        for(CoinEnum c : change) {
            coinCashInventory.deduct(c);
        }
    }
}
