package com.challenge.vendingmachine.crud;

import com.challenge.vendingmachine.entity.CoinEnum;

import java.util.List;
import java.util.Map;

public interface VendingMachine {

    void userInsertCoin(CoinEnum coinEnum);
    List<CoinEnum> calculateChange(long productPrice);
    long getCurrentBalance();
    Map<CoinEnum, Integer> getCoinInventory();

}
