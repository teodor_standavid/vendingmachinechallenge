package com.challenge.vendingmachine.controller;

import com.challenge.vendingmachine.crud.VendingMachine;
import com.challenge.vendingmachine.crud.VendingMachineImpl;
import com.challenge.vendingmachine.entity.CoinEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestAttributes;

import java.util.Map;

@Slf4j
@RestController
@RequestMapping(path = "/vendingmachine")
public class VendingController {

    VendingMachine machine = new VendingMachineImpl();

    @GetMapping(path="/balance", produces = "application/json")
    public long balance() {
        log.info("Balance in machine {}",machine.getCurrentBalance());
        return machine.getCurrentBalance();
    }

    @GetMapping(path="/coins", produces = "application/json")
    public Map<CoinEnum, Integer> getCoinInventory() {
        log.info("Coins in machine {}",machine.getCoinInventory());
        return machine.getCoinInventory();
    }

    @PostMapping(path= "/insertcoin")
    public void insertCoin(@RequestBody String coinbody) {
//        ObjectMapper objectMapper = new ObjectMapper();
//        String test = objectMapper.writeValueAsString(CoinEnum.TENPENCE);
//        CoinEnum coin = objectMapper.readValue(coinbody, CoinEnum.class);
        CoinEnum coin = CoinEnum.valueOf(coinbody);
        log.info("Coin {} inserted", String.valueOf(coin));
        machine.userInsertCoin(coin);

    }
    @PostMapping(path= "/calculatechange")
    public void insertCoin(@RequestBody String coinbody) {
//        ObjectMapper objectMapper = new ObjectMapper();
//        String test = objectMapper.writeValueAsString(CoinEnum.TENPENCE);
//        CoinEnum coin = objectMapper.readValue(coinbody, CoinEnum.class);
        CoinEnum coin = CoinEnum.valueOf(coinbody);
        log.info("Coin {} inserted", String.valueOf(coin));
        machine.userInsertCoin(coin);

    }
}
