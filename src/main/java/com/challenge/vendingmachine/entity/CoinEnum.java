package com.challenge.vendingmachine.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

// The coins accepted by the vending machine
//@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum CoinEnum {

    FIVEPENCE(5),
    TENPENCE(10),
    TWENTYPENCE(20),
    FIFTYPENCE(50),
    ONEPOUND(100),
    TWOPOUNDS(200);
//    @Getter
    private int denomitation;

    private CoinEnum(int denomitation) {
        this.denomitation = denomitation;
    }

    public int  getDenomitation(){
        return denomitation;
    }
}
